import Design.login
import Mylib.lib
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QMessageBox
class Login(QWidget,Design.login.Ui_Form):
    def __init__(self):
        super(Login,self).__init__()
        Login.setupUi(self,self)
        self.pushButton_login.clicked.connect(self.__pushButton_login_clicked)
        self.pushButton_register.clicked.connect(self.__pushButton_register_clicked)

    def get_ip(self):
        my_ip = self.comboBox_chooseServer.currentText()
        if my_ip != "":
            return my_ip
        return False

    def my_login(self):
        user_name = self.lineEdit_username.text()
        user_password = self.lineEdit_password.text()
        #mes = QMessageBox()
        #mes.setStyleSheet(
            #'QMessageBox {background-color: yellow; color: white}')
        #mes.show()
        #create login info(a dict)
        if self.judgeInput(user_name,user_password):
            the_info = Mylib.lib.creatSendInfo(user_name,user_password,True)
            the_ip = self.get_ip()
        #default ip is my server ip; default port is 80
            sk = Mylib.lib.createSocketConnection(the_ip)
            if sk != False:
                Mylib.lib.send(sk,the_info)
                QMessageBox.information(self, '提示', '发送成功', QMessageBox.Yes)
                #QMessageBox.show()
                return sk
            QMessageBox.information(self, '提示', '连接失败', QMessageBox.Yes)
            #QMessageBox.show()
            return False
        else:
            QMessageBox.information(self, '提示', '请输入用户名和密码', QMessageBox.Yes)
            #QMessageBox.setText("请输入用户名和密码！")
            #QMessageBox.show()
            return False

    def my_register(self):
        if self.lineEdit_password_register.text() != self.lineEdit_password_again.text():
            QMessageBox.information(self, '提示', '请重新输入密码', QMessageBox.Yes)
            #QMessageBox.show()
            return False
        user_name = self.lineEdit_username_register.text()
        user_password = self.lineEdit_password_register.text()
        if self.judgeInput(user_name,user_password):
            the_info = Mylib.lib.creatSendInfo(user_name,user_password,False)
            the_ip = self.get_ip()
            sk = Mylib.lib.createSocketConnection(the_ip)
            if sk != False:
                Mylib.lib.send(sk,the_info)
                QMessageBox.information(self, '提示', '发送成功', QMessageBox.Yes)
                #QMessageBox.show()
                return sk
            QMessageBox.information(self, '提示', '连接失败', QMessageBox.Yes)
            #QMessageBox.show()
            return False
        else:
            QMessageBox.information(self, '提示', '请输入用户名和密码', QMessageBox.Yes)
            #QMessageBox.setText("请输入用户名和密码！")
            #QMessageBox.show()
            return False

    def recv_message(self,sk):
        message = Mylib.lib.receiveMessage(sk)
        QMessageBox.information(self, '提示',message, QMessageBox.Yes)
        #QMessageBox.setText(message)
        #QMessageBox.show()
        return True

    def judgeInput(self,username,password):
        if username == "":
            return False
        if password == "":
            return False
        return True

    def __pushButton_login_clicked(self):
        sk = self.my_login()
        if sk != False:
            self.recv_message(sk)
            sk.close()

    def __pushButton_register_clicked(self):
        sk = self.my_register()
        if sk != False:
            self.recv_message(sk)
            sk.close()