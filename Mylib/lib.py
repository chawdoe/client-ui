import json
import socket
import os
import lib
def infoToJson(data):
    content = json.dumps(data)
    return content

def jsonToBinary(content):
    return content.encode("utf-8")

def toBinary(content):
    return jsonToBinary(infoToJson(content))

def binaryToJson(content):
    return content.decode("utf-8")

def jsonToInfo(content):
    data = json.loads(content)
    return data

def toInfo(content):
    return jsonToInfo(binaryToJson(content))

def send(sk,theData):
    sk.sendall(toBinary(theData))
    return True

def recv(sk):
    temp = sk.recv(1024)
    if not temp:
        return False
    return toInfo(temp)

def receiveMessage(sk):
    temp = sk.recv(1024)
    if not temp:
        return False
    return temp.decode('utf-8')

def creatSendInfo(username, password, is_login = False):
    the_info = {"username": "", "password": "", "is_login": False}
    the_info["username"] = username
    the_info["password"] = password
    the_info["is_login"] = is_login
    return the_info

def createSocketConnection(the_ip = "139.199.179.136",the_port = 80):
    sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sk.connect((the_ip, the_port))
        return sk
    except OSError:
        return False

def SaveOnline(username,file):
    sk = createSocketConnection()
    if sk == False:
        return False
    path = lib.get_file(file)
    flag = "save" + file
    if os.path.exists(path) is True:
        newfile = open(path, 'r')
        content = newfile.read()
        dict_info = {"username":username,"flag":flag,"content":content}

        send(sk,dict_info)
        recv_message = recv(sk)

        sk.close()
        newfile.close()
        return recv_message
    return False

def LoadOnline(username,file):
    sk = createSocketConnection()
    if sk == False:
        return False
    path = lib.get_file(file)
    flag = "load" + file
    if os.path.exists(path) is True:
        newfile = open(path,"w+")
        dict_info = {"username":username,"flag":flag,"content":""}

        send(sk,dict_info)
        temp = sk.recv(4096)

        recv_info = binaryToJson(temp)
        remp = {ord('\\'):None,ord('['):None,ord(']'):None}
        a = recv_info.translate(remp)
        json_info = a[1:-1]

        newfile.write(json_info)
        sk.close()
        newfile.close()
        return True
    return False

def closeConnection():
    sk = createSocketConnection()
    if sk == False:
        return False
    dict_info = {"close": True}
    send(sk,dict_info)
    recv_info = receiveMessage(sk)
    sk.close()
    return recv_info
