import  sys
def get_rate(a, b):
    if b != 0:
        temp = float(a / b * 100.0)
        temp2 = int(temp*100)
        temp = temp2 / 100.0
        result = str(temp) + "%"
    else:
        result = "0%"
    return result


def get_average(a, b):
    if b != 0:
        temp = float(a / b)
        temp2 = int(temp*100)
        temp = temp2 / 100.0
    else:
        temp = 0
    return str(temp)

def get_file(name):
        path = sys.path[0]
        myfile = path + "/temp_asset/" + name + ".json"
        return myfile